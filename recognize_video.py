# USAGE
# python recognize_video.py --detector face_detection_model \
#	--embedding-model openface_nn4.small2.v1.t7 \
#	--recognizer output/recognizer.pickle \
#	--le output/le.pickle

# import the necessary packages
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import pickle
import time
import cv2
import os
import datetime
import logging

def send_mail(name,address,time,img,folder):
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.image import MIMEImage
    import smtplib
    from email.mime.base import MIMEBase
    from email import encoders

    fromaddr = "mennahjafar@hotmail.com"
    toaddr = ["mennahjafar@hotmail.com"]

    # instance of MIMEMultipart
    msg = MIMEMultipart()

    # storing the senders email address
    msg['From'] = fromaddr

    # storing the receivers email address
    msg['To'] = ""

    # storing the subject
    msg['Subject'] = "Test"

    # string to store the body of the mail
    body = "{} was detected in loc {} at time {}".format(name,address,time)

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # open the file to be sent
    filename = os.path.join(folder,img)
    attachment = open(filename, "rb")

    # instance of MIMEBase and named as p
    p = MIMEBase('application', 'octet-stream')

    # To change the payload into encoded form
    p.set_payload((attachment).read())

    # encode into base64
    encoders.encode_base64(p)

    p.add_header('Content-Disposition', "attachment; filename= %s" % filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # creates SMTP session
    s = smtplib.SMTP('smtp-mail.outlook.com', 587)

    # start TLS for security
    s.starttls()

    # Authentication
    s.login(fromaddr, "skyfallM4")

    # Converts the Multipart msg into a string
    text = msg.as_string()

    # sending the mail
    s.sendmail(fromaddr, toaddr, text)

    # terminating the session
    s.quit()




# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--detector", required=True,
	help="path to OpenCV's deep learning face detector")
ap.add_argument("-m", "--embedding-model", required=True,
	help="path to OpenCV's deep learning face embedding model")
ap.add_argument("-r", "--recognizer", required=True,
	help="path to model trained to recognize faces")
ap.add_argument("-l", "--le", required=True,
	help="path to label encoder")
ap.add_argument("-c", "--confidence", type=float, default=0.5,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

# load our serialized face detector from disk
print("[INFO] loading face detector...")
protoPath = os.path.sep.join([args["detector"], "deploy.prototxt"])
modelPath = os.path.sep.join([args["detector"],
	"res10_300x300_ssd_iter_140000.caffemodel"])
detector = cv2.dnn.readNetFromCaffe(protoPath, modelPath)

# load our serialized face embedding model from disk
print("[INFO] loading face recognizer...")
embedder = cv2.dnn.readNetFromTorch(args["embedding_model"])

# load the actual face recognition model along with the label encoder
recognizer = pickle.loads(open(args["recognizer"], "rb").read())
le = pickle.loads(open(args["le"], "rb").read())

# initialize the video stream, then allow the camera sensor to warm up
print("[INFO] starting video stream...")
src=0
vs = VideoStream(src).start()
time.sleep(2.0)

# start the FPS throughput estimator
fps = FPS().start()

count = 0
# loop over frames from the video file stream
while True:
	# grab the frame from the threaded video stream
	frame = vs.read()
	frame_expanded = np.expand_dims(frame, axis=0)

	current_time = datetime.datetime.now()
	ts = current_time.strftime("%A %d %B %Y %I:%M:%S%p")
	width , height, channel = frame.shape
	cv2.putText(frame, ts, (10, 100), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255,255,255), 3)

	# resize the frame to have a width of 600 pixels (while
	# maintaining the aspect ratio), and then grab the image
	# dimensions
	frame = imutils.resize(frame, width=600)
	(h, w) = frame.shape[:2]

	# construct a blob from the image
	imageBlob = cv2.dnn.blobFromImage(
		cv2.resize(frame, (300, 300)), 1.0, (300, 300),
		(104.0, 177.0, 123.0), swapRB=False, crop=False)

	# apply OpenCV's deep learning-based face detector to localize
	# faces in the input image
	detector.setInput(imageBlob)
	detections = detector.forward()
	print("Detections:", detections.shape[2])

	# loop over the detections
	for i in range(0, detections.shape[2]):
		# extract the confidence (i.e., probability) associated with
		# the prediction
		confidence = detections[0, 0, i, 2]

		# filter out weak detections
		if confidence > args["confidence"]:
			# compute the (x, y)-coordinates of the bounding box for
			# the face
			box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")

			# extract the face ROI
			face = frame[startY:endY, startX:endX]
			(fH, fW) = face.shape[:2]

			# ensure the face width and height are sufficiently large
			if fW < 20 or fH < 20:
				continue

			# construct a blob for the face ROI, then pass the blob
			# through our face embedding model to obtain the 128-d
			# quantification of the face
			faceBlob = cv2.dnn.blobFromImage(face, 1.0 / 255,
				(96, 96), (0, 0, 0), swapRB=True, crop=False)
			embedder.setInput(faceBlob)
			vec = embedder.forward()

			# perform classification to recognize the face
			preds = recognizer.predict_proba(vec)[0]
			j = np.argmax(preds)
			proba = preds[j]
			name = le.classes_[j]
			print("name:", name)
			#print("fbs:", fbs)

			if name != "unknown":
				cv2.putText(frame, 'alarm!!!', (100,100) , cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,0,255), 3)
				if "New_Unknown_{}".format(name) not in os.listdir("/home/mennah/Desktop/opencv-face-recognition"):
					os.mkdir("New_Unknown_{}".format(name))

				src = 0
				ts = current_time.strftime("%A %d %B %Y %I:%M:%S%p")
				os.chdir("New_Unknown_{}".format(name))
				# Create a custom logger
				logger = logging.getLogger(__name__)
				#if "{}.log".format(name) not in os.listdir():
					# Create a custom logger
					#logger = logging.getLogger(__name__)
					# Create handlers
				c_handler = logging.StreamHandler()
				c_handler = logging.FileHandler('{}.log'.format(name))
				c_handler.setLevel(logging.WARNING)
					# Create formatters and add it to handlers
				c_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
				c_handler.setFormatter(c_format)
				#else:
				logger.addHandler(c_handler)
				logger.warning("the camera number {} had watched {} at time {}".format(src,name,ts))
				cv2.imwrite("name_%d.jpg"%count, frame)
				os.chdir("..")
				send_mail(name,src,ts,"name_%d.jpg"%count,"New_Unknown_{}".format(name))
				count = count + 1



			# draw the bounding box of the face along with the
			# associated probability
			text = "{}: {:.2f}%".format(name, proba * 100)
			y = startY - 10 if startY - 10 > 10 else startY + 10
			cv2.rectangle(frame, (startX, startY), (endX, endY),
				(0, 0, 255), 2)
			cv2.putText(frame, text, (startX, y),
				cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)

	# update the FPS counter
	fps.update()

	# show the output frame
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF

	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break

# stop the timer and display FPS information
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
vs.release()
cv2.destroyAllWindows()
vs.stop()
