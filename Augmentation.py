#data generator 
import os
from os import listdir
from os.path import isfile, join
import numpy
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import cv2

for each_folder in os.listdir("dataset"):
    print(os.getcwd())
    mypath=os.path.join("dataset/","{}".format(each_folder))
    print(mypath)
    onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
    images = numpy.empty(len(onlyfiles), dtype=object)
    print (len(onlyfiles))
    for n in range(0, len(onlyfiles)):
      images[n] = cv2.imread( join(mypath,onlyfiles[n]) )
      datagen = ImageDataGenerator(
              rotation_range=5,
		      # apply ZCA whitening
		      zca_whitening=False,
            # epsilon for ZCA whitening
              zca_epsilon=1e-06,
		      height_shift_range=0.1,
		    # set range for random shear
		      shear_range=0.,
		    # set range for random zoom
		      zoom_range=0.,
		    # set range for random channel shifts
		      channel_shift_range=0.
		      
              )

      x = img_to_array(images[n])  # this is a Numpy array with shape (3, 150, 150)
      x = x.reshape((1,) + x.shape)  # this is a Numpy array with shape (1, 3, 150, 150)

      # the .flow() command below generates batches of randomly transformed images
      # and saves the results to the `preview/` directory
      i = 0
      for batch in datagen.flow(x, batch_size=1,save_to_dir=mypath, save_prefix='LP_', save_format='jpg'):
          i += 1
          if i >= 10:
              break  # otherwise the generator would loop indefinitely
                        
     
